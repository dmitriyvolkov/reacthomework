import { render, fireEvent, screen } from '@testing-library/react'
import Modal from './Modal'

describe('Modal', () => {
  test('render header modal', () => {
    const headerText = 'Modal Header'
    render(<Modal header={headerText} />)
    expect(screen.getByText(headerText)).toBeInTheDocument()
  })

  test('renders modal text', () => {
    const modalText = 'Modal Text'
    render(<Modal text={modalText} />)
    expect(screen.getByText(modalText)).toBeInTheDocument()
  })

  test('renders modal actions', () => {
    const actions = <button>OK</button>
    render(<Modal actions={actions} />)
    expect(screen.getByRole('button', { name: 'OK' })).toBeInTheDocument()
  })
})
