import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from '../../store/actions'
import Buttons from '../Buttons'
import Input from './Input/Input'
import { validationSchema } from './validations'
import './Form.scss'

const Form = () => {
  const dispatch = useDispatch()
  const cart = useSelector((store) => store.shopingCart)
  const objDelivery = useSelector((store) => store.objDelivery)
  const formikForm = useFormik({
    initialValues: objDelivery,
    validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      dispatch(actions.actionsObjectDelivery({ user: values, products: cart }))
      console.log(
        actions.actionsObjectDelivery({ user: values, products: cart }),
      )
      resetForm()
    },
  })

  return (
    <div className="page">
      <form onSubmit={formikForm.handleSubmit}>
        <Input
          {...formikForm.getFieldProps('name')}
          type="text"
          classNames="mb-3"
          name="name"
          placeholder="name"
          label={'Name'}
          error={formikForm.errors.name && formikForm.touched.name}
          errorMessage={formikForm.errors.name}
        />
        <Input
          {...formikForm.getFieldProps('lastName')}
          type="text"
          classNames="mb-3"
          name="lastName"
          placeholder="lastName"
          label={'lastName'}
          error={formikForm.errors.lastName && formikForm.touched.lastName}
          errorMessage={formikForm.errors.lastName}
        />
        <Input
          {...formikForm.getFieldProps('age')}
          type="text"
          classNames="mb-3"
          name="age"
          placeholder="age"
          label={'age'}
          error={formikForm.errors.age && formikForm.touched.age}
          errorMessage={formikForm.errors.age}
        />
        <Input
          {...formikForm.getFieldProps('adress')}
          type="text"
          classNames="mb-3"
          name="adress"
          placeholder="adress delivery"
          label={'adress delivery'}
          error={formikForm.errors.adress && formikForm.touched.adress}
          errorMessage={formikForm.errors.adress}
        />
        <Input
          {...formikForm.getFieldProps('phone')}
          type="tel"
          classNames="mb-3"
          name="phone"
          placeholder="phone"
          label={'phone'}
          error={formikForm.errors.phone && formikForm.touched.phone}
          errorMessage={formikForm.errors.phone}
        />
        <div className="col-12">
          <Buttons
            type={'submit'}
            text={'Checkout'}
            backgroundColor="#000"
          ></Buttons>
        </div>
      </form>
    </div>
  )
}

export default Form
