import * as yup from 'yup'

export const validationSchema = yup.object({
  name: yup
    .string()
    .required('Поле должно быть обезательным')
    .min(2, 'min 2')
    .max(16, 'max 16')
    .matches(/[a-zA-Z]/, 'only letters'),
  phone: yup
    .string()
    .required('Поле должно быть обезательным')
    .min(11, 'min 11')
    .max(11, 'max 11')
    .matches(/[0-9]/, 'only numbers'),
  email: yup
    .string()
    .email('Enter your Email')
    .matches(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      'точка где?',
    ),
  age: yup.number().required('Поле должно быть обезательным'),
})
