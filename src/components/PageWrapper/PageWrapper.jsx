import Header from '../Header/Header'
const PageWrapper = (props) => {
  const { children, star, cart } = props
  return (
    <div>
      <Header star={star} cart={cart} />
      {children}
    </div>
  )
}
export default PageWrapper
