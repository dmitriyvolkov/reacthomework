import PropTypes from 'prop-types'
import './Button.scss'
import cx from 'classnames'

const Buttons = ({
  onClick,
  classNames,
  btnActive,
  backgroundColor,
  text,
  isDisabled,
  type,
  width,
}) => {
  return (
    <button
      className={cx('button', classNames, { active: btnActive })}
      onClick={onClick}
      style={{ backgroundColor: backgroundColor, width: width }}
      disabled={isDisabled}
      type={type}
    >
      {text}
    </button>
  )
}

Buttons.propTypes = {
  onClick: PropTypes.func,
  classNames: PropTypes.string,
  btnActive: PropTypes.bool,
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  isDisabled: PropTypes.bool,
  width: PropTypes.string,
}

export default Buttons
