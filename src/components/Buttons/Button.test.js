
import { render, fireEvent, screen } from '@testing-library/react'
import Buttons from './Buttons'

describe('Buttons', () => {
  test('Render button in the documents', () => {
    const buttonText = 'Click me'
    render(<Buttons text={buttonText} />)
    expect(screen.getByText(buttonText)).toBeInTheDocument()
  })

  test('Use class in button', () => {
    const additionalClass = 'custom-class'
    render(<Buttons classNames={additionalClass} />)
    expect(screen.getByRole('button')).toHaveClass('custom-class')
  })

  test('test on click', () => {
    const handleClick = jest.fn()
    render(<Buttons onClick={handleClick} />)
    fireEvent.click(screen.getByRole('button'))
    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  test('test on type in button ', () => {
    const type = 'submit'
    render(<Buttons type={type} />)
    expect(screen.getByRole('button')).toHaveAttribute('type', type)
  })
})
