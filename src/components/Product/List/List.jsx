import { useContext } from 'react'
import PropTypes from 'prop-types'
import Card from '../Cards/Card'
import { ProductViewModeContext } from '../../../Pages/MainPages/ProductListContext'
import './list.scss'
const List = ({
  addFavorites,
  removeFavorites,
  openModal,
  addReadyToCart,
  button,
  closeImg,
  redyToRemoveCart,
  products,
}) => {
  const { viewMode } = useContext(ProductViewModeContext)

  const arr = products.map((card) => {
    return (
      <Card
        key={card.id}
        card={card}
        addFavorite={addFavorites}
        removeFavorites={removeFavorites}
        openModal={openModal}
        addReadyToCart={addReadyToCart}
        button={button}
        closeImg={closeImg}
        redyToRemoveCart={redyToRemoveCart}
      />
    )
  })
  return (
    <>
      {viewMode === 'cards' ? (
        <div className="wrapper">{arr}</div>
      ) : (
        <div className="wrapper__list">{arr}</div>
      )}
    </>
  )
}
List.propTypes = {
  addFavorites: PropTypes.func,
  removeFavorites: PropTypes.func,
  openModal: PropTypes.func,
  addReadyToCart: PropTypes.func,
}
export default List
