import cx from 'classnames'
import { ReactComponent as Star } from './img/star.svg'
import './card.scss'
import Buttons from '../../Buttons'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from '../../../store/actions'
import * as selectore from '../../../store/selectore'

const Card = ({ card, addReadyToCart, button, closeImg, redyToRemoveCart }) => {
  const dispatch = useDispatch()
  const favorites = useSelector(selectore.selectFavorite)

  const { image, price, vendorСode, color, name, id } = card

  const isFavorite = favorites?.some((item) => item.id === id)

  const openModal = () => {
    dispatch(actions.actionsOpenModal(true))
  }

  const handlerRemove = (item) => {
    const isFavorite = favorites.some((favorite) => favorite.id === item.id)
    console.log(isFavorite)

    if (isFavorite) {
      dispatch(actions.actionWishListRemove(item))
      const newFavorites = JSON.parse(localStorage.getItem('favorites')).filter(
        (item) => item.id !== id,
      )
      localStorage.setItem('favorites', JSON.stringify(newFavorites))
    } else {
      dispatch(actions.actionWishList(item))
      localStorage.setItem('favorites', JSON.stringify([...favorites, item]))
    }
  }

  return (
    <div className="productCard">
      <img className="img" src={image} alt="" />
      <h1 className="title">{name}</h1>
      <p className="price">Цена: {price} $</p>
      <p className="vendor-code">Артикул: {vendorСode}</p>
      <p>Цвет: {color}</p>

      {!closeImg ? (
        <span
          className={cx('star', { 'star--active': isFavorite })}
          onClick={() => {
            handlerRemove(card)
          }}
        >
          <Star />
        </span>
      ) : (
        <span
          className={'close'}
          onClick={() => {
            redyToRemoveCart(card)
            openModal()
          }}
        >
          {closeImg}
        </span>
      )}

      {!button ? (
        <Buttons
          onClick={() => {
            addReadyToCart(card)
            openModal()
          }}
          text={'Add to cart'}
          classNames="button--size"
        />
      ) : (
        <span></span>
      )}
    </div>
  )
}
export default Card
