import { useEffect, useContext } from 'react'
import Modal from '../../components/Modal'
import List from '../../components/Product/List/List'
import Buttons from '../../components/Buttons'
import PageWrapper from '../../components/PageWrapper'
import { useDispatch, useSelector } from 'react-redux'
import { ProductViewModeContext } from '../MainPages/ProductListContext'
import * as actions from '../../store/actions'
import * as selectore from '../../store/selectore'
import './FavoritePages.scss'

const FavoritePages = () => {
  const dispatch = useDispatch()
  const favorites = useSelector(selectore.selectFavorite)
  const { viewMode, toggleViewMode } = useContext(ProductViewModeContext)
  const cart = useSelector((store) => store.shopingCart)
  const readyToCart = useSelector((store) => store.addRedyToCard)

  const modal = useSelector((store) => store.modal)
  useEffect(() => {
    const favoritesLockalStorage =
      JSON.parse(localStorage.getItem('favorites')) || []
    const cartLockalStorage =
      JSON.parse(localStorage.getItem('shopingCart')) || []
    if (favoritesLockalStorage.length > 0 && favorites.length === 0) {
      dispatch(actions.actionWishListLocalSor(favoritesLockalStorage))
    }
    if (cartLockalStorage.length > 0 && cart.length === 0) {
      dispatch(actions.actionShopingCart(cartLockalStorage))
    }
  }, [dispatch, favorites, cart])

  const handlerRemove = (item, id) => {
    const isFavorite = favorites.find((favorite) => favorite.id === item.id)

    if (isFavorite) {
      dispatch(actions.actionWishListRemove(item))
      const newFavorites = favorites.filter((item) => item.id !== id.id)
      localStorage.setItem('favorites', JSON.stringify(newFavorites))
    } else {
      dispatch(actions.actionWishList(item))
      localStorage.setItem('favorites', JSON.stringify([...favorites, item]))
    }
  }

  const handlerShoppingCart = (product, id) => {
    const isCart = cart.find((favorite) => favorite.id === product.id)
    if (isCart) {
      const updatedCart = cart.filter((card) => card.id !== product.id)
      localStorage.setItem('shopingCart', JSON.stringify(updatedCart))
    } else {
      dispatch(actions.actionShopingCart(product))
    }
  }

  const addReadyToCart = (card) => {
    dispatch(actions.addRedyToCard(card))
  }

  const openModal = () => {
    dispatch(actions.actionsOpenModal(true))
  }
  const closeModal = () => {
    dispatch(actions.actionsOpenModal(false))
  }
  return (
    <>
      <PageWrapper star={favorites.length} cart={cart.length}>
        {favorites.length !== 0 ? (
          <main className="main">
            <div className="btn-wrapper">
              <Buttons
                onClick={toggleViewMode}
                text="Cards / List"
                backgroundColor="black"
                classNames="toggle-list"
              />
            </div>
            {viewMode === 'cards' ? (
              <List
                addFavorites={handlerRemove}
                openModal={openModal}
                addToCart={handlerShoppingCart}
                addReadyToCart={addReadyToCart}
                products={favorites}
                buttontext={'Add to cart'}
              />
            ) : (
              <List
                addFavorites={handlerRemove}
                openModal={openModal}
                addToCart={handlerShoppingCart}
                addReadyToCart={addReadyToCart}
                products={favorites}
                buttontext={'Add to cart'}
              />
            )}
            {modal && (
              <Modal
                text="Вы точно хотите добавить в корзину?"
                header="Подтвердение"
                close={openModal}
                actions={
                  <>
                    <Buttons
                      onClick={() => {
                        handlerShoppingCart(readyToCart)
                        closeModal()
                      }}
                      text={'Добавить'}
                      backgroundColor="#d3c1d9"
                    />
                    <Buttons
                      onClick={closeModal}
                      backgroundColor="#d3c1d9"
                      text={'Отмена'}
                    />
                  </>
                }
              />
            )}
          </main>
        ) : (
          <span className="error">Sorry, you dont have favorites shoes</span>
        )}
      </PageWrapper>
    </>
  )
}

export default FavoritePages
