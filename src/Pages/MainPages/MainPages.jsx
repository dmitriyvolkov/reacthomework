import { useEffect, useContext } from 'react'
import Modal from '../../components/Modal'
import List from '../../components/Product/List/List'
import Buttons from '../../components/Buttons'
import PageWrapper from '../../components/PageWrapper'
import { useDispatch, useSelector } from 'react-redux'
import * as actions from '../../store/actions'
import { ProductViewModeContext } from './ProductListContext'
import './MainPages.scss'
const App = () => {
  const { viewMode, toggleViewMode } = useContext(ProductViewModeContext)
  const favorites = useSelector((store) => store.wishList)
  const cart = useSelector((store) => store.shopingCart)
  const modal = useSelector((store) => store.modal)
  const products = useSelector((store) => store.data)
  const redyToCart = useSelector((store) => store.addRedyToCard)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.actionsFetchProduct())
    const favoritesLockalStorage =
      JSON.parse(localStorage.getItem('favorites')) || []
    const cartLockalStorage =
      JSON.parse(localStorage.getItem('shopingCart')) || []
    if (favoritesLockalStorage.length > 0 && favorites.length === 0) {
      dispatch(actions.actionWishListLocalSor(favoritesLockalStorage))
    }
    if (cartLockalStorage.length > 0 && cart.length === 0) {
      dispatch(actions.actionShopingCart(cartLockalStorage))
    }
  }, [dispatch, cart, favorites])

  const handlerShoppingCart = (product, id) => {
    const isCart = cart.find((favorite) => favorite.id === product.id)
    if (isCart) {
      const updatedCart = cart.filter((card) => card.id !== product.id)
      localStorage.setItem('shopingCart', JSON.stringify(updatedCart))
      console.log('if')
    } else {
      localStorage.setItem('shopingCart', JSON.stringify([...cart, product]))
      dispatch(actions.actionShopingCart(product))
      console.log('else')
    }
  }

  const addReadyToCart = (card) => {
    dispatch(actions.addRedyToCard(card))
  }
  const closeModal = () => {
    dispatch(actions.actionsOpenModal(false))
  }

  return (
    <>
      <PageWrapper star={favorites.length} cart={cart.length}>
        <main className="main">
          <div className="btn-wrapper">
            <Buttons
              onClick={toggleViewMode}
              text="Cards / List"
              backgroundColor="black"
              classNames="toggle-list"
            />
          </div>

          {viewMode === 'cards' ? (
            <List
              addReadyToCart={addReadyToCart}
              products={products}
              buttontext={'Add to cart'}
            />
          ) : (
            <List
              addReadyToCart={addReadyToCart}
              products={products}
              buttontext={'Add to cart'}
            />
          )}

          {modal && (
            <Modal
              text="Вы точно хотите добавить в корзину?"
              header="Подтвердение"
              close={closeModal}
              actions={
                <>
                  <Buttons
                    onClick={() => {
                      handlerShoppingCart(redyToCart)
                      closeModal()
                    }}
                    text={'Добавить'}
                    backgroundColor="#d3c1d9"
                  />
                  <Buttons
                    onClick={closeModal}
                    backgroundColor="#d3c1d9"
                    text={'Отмена'}
                  />
                </>
              }
            />
          )}
        </main>
      </PageWrapper>
    </>
  )
}

export default App
