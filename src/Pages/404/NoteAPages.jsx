import './NoteAPages.scss'

const NotPage = () => {
  return (
    <div className="not-page">
      <div>
        <p className="page-title">This page has been abducted.</p>
        <p className="page-desc">404</p>
        <p>
          <a href="/" class="link">
            Go to the Shose store
          </a>
        </p>
      </div>
    </div>
  )
}
export default NotPage
