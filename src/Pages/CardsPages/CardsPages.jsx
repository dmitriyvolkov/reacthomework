import { useContext } from 'react'
import PageWrapper from '../../components/PageWrapper'
import List from '../../components/Product/List/List'
import Buttons from '../../components/Buttons'
import Modal from '../../components/Modal'
import { ReactComponent as Close } from './icons/cross.svg'
import { useDispatch, useSelector } from 'react-redux'
import { ProductViewModeContext } from '../MainPages/ProductListContext'
import Form from '../../components/Form/Form'
import * as actions from '../../store/actions'
import { useEffect } from 'react'
import './CardsPages.scss'

const CardPages = () => {
  const dispatch = useDispatch()
  const modal = useSelector((store) => store.modal)
  const modalForm = useSelector((store) => store.modal2)
  const setIsRemovedCard = useSelector((store) => store.addRemoveCard)
  const favorites = useSelector((store) => store.wishList)
  const { viewMode, toggleViewMode } = useContext(ProductViewModeContext)
  const cart = useSelector((store) => store.shopingCart)

  useEffect(() => {
    const favoritesLockalStorage =
      JSON.parse(localStorage.getItem('favorites')) || []
    const cartLockalStorage =
      JSON.parse(localStorage.getItem('shopingCart')) || []
    if (favoritesLockalStorage.length > 0 && favorites.length === 0) {
      dispatch(actions.actionWishListLocalSor(favoritesLockalStorage))
    }
    if (cartLockalStorage.length > 0 && cart.length === 0) {
      dispatch(actions.actionShopingCart(cartLockalStorage))
    }
  }, [dispatch, favorites, cart])
  const removeCart = (card) => {
    const newCart = cart.filter((item) => item.id !== card.id)
    dispatch(actions.actionShopingCartRemove(card))
    localStorage.setItem('shopingCart', JSON.stringify(newCart))
  }
  const openModal = () => {
    dispatch(actions.actionsOpenModal(true))
  }
  const handelModalForm = () => {
    dispatch(actions.actionsModalForm(!modalForm))
  }
  const closeModal = () => {
    dispatch(actions.actionsOpenModal(false))
  }

  const addRemoveToCard = (card) => {
    dispatch(actions.addRemoveCard(card))
  }
  return (
    <PageWrapper star={favorites.length} cart={cart.length}>
      {cart.length !== 0 ? (
        <main className="main">
          <div className="cardpage-wrapper-btn">
            <Buttons
              classNames={'btn-bynow'}
              onClick={() => {
                handelModalForm()
              }}
              text={'By Now'}
              backgroundColor="#3333"
            />
            <Buttons
              onClick={toggleViewMode}
              text="Cards / List"
              backgroundColor="black"
              classNames="toggle-list"
            />
          </div>
          {viewMode === 'cards' ? (
            <List
              openModal={openModal}
              redyToRemoveCart={addRemoveToCard}
              products={cart}
              button={'button'}
              closeImg={<Close />}
            />
          ) : (
            <List
              openModal={openModal}
              redyToRemoveCart={addRemoveToCard}
              products={cart}
              button={'button'}
              closeImg={<Close />}
            />
          )}

          {modal && (
            <Modal
              text="Вы точно хотите удалить с корзины?"
              header="Подтвердение"
              close={closeModal}
              actions={
                <>
                  <Buttons
                    onClick={() => {
                      removeCart(setIsRemovedCard)
                      closeModal()
                    }}
                    text={'Удалить'}
                    backgroundColor="#d3c1d9"
                  />
                  <Buttons
                    onClick={closeModal}
                    backgroundColor="#d3c1d9"
                    text={'Отмена'}
                  />
                </>
              }
            />
          )}
          {modalForm && (
            <Modal text={<Form />} header="Form BY" close={handelModalForm} />
          )}
        </main>
      ) : (
        <span className="error">Sorry your cart is empty</span>
      )}
    </PageWrapper>
  )
}

export default CardPages
