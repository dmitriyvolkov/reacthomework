import { Routes, Route } from 'react-router-dom'
import Main from '../Pages/MainPages'
import FavoritePages from '../Pages/FavoritePages'
import CardsPages from '../Pages/CardsPages'
import NotPage from '../Pages/404/NoteAPages'
const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Main />}></Route>
      <Route path="/star" element={<FavoritePages />}></Route>
      <Route path="/cart" element={<CardsPages />}></Route>
      <Route path="*" element={<NotPage />}></Route>
    </Routes>
  )
}
export default AppRoutes
