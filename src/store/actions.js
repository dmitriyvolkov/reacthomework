import { createAction } from '@reduxjs/toolkit'
import sendRequest from '../helpers/sendRequest'
export const actionsOpenModal = createAction('ACTIONS_OPEN_MODAL')
export const actionsCloseModal = createAction('ACTIONS_CLOSE_MODAL')
export const actionsProductData = createAction('ACTIONS_PRODUCT_DATA')
export const actionWishList = createAction('ACTION_WISH_LIST')
export const actionWishListLocalSor = createAction('actionWishListLocalSor')
export const actionWishListRemove = createAction('ACTION_WISH_LIST_REMOVE')
export const actionShopingCart = createAction('ACTION_SHOPING_CART')
export const actionsModalForm = createAction('ACTIONS_MODAL_FORM')
export const actionsObjectDelivery = createAction('ACTIONS_OBJECT_DELIVERY')
export const actionShopingCartRemove = createAction(
  'ACTION_SHOPING_CART_REMOVE',
)
export const addRedyToCard = createAction('ADD_REDY_TO_CARD')
export const addRemoveCard = createAction('ADD_REMOVE_CARD')

export const actionsFetchProduct = () => (dispatch) => {
  sendRequest('productCards.json').then((data) => {
    dispatch(actionsProductData(data))
  })
}
