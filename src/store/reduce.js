import { createReducer } from '@reduxjs/toolkit'
import * as actions from './actions'

export const initialState = {
  data: [],
  wishList: [],
  shopingCart: [],
  modal: false,
  modal2: false,
  addRedyToCard: [],
  addRemoveCard: {},
  objDelivery: {
    user: { name: '', lastName: '', age: '', adress: '', phone: '' },
    product: [],
  },
}

export default createReducer(initialState, (builder) => {
  builder
    .addCase(actions.actionsProductData, (state, { payload }) => {
      state.data = payload
    })
    .addCase(actions.actionsOpenModal, (state, { payload }) => {
      state.modal = payload
    })
    .addCase(actions.actionsCloseModal, (state, { payload }) => {
      state.modal = payload
    })
    .addCase(actions.actionsModalForm, (state, { payload }) => {
      state.modal2 = payload
    })
    .addCase(actions.actionShopingCart, (state, { payload }) => {
      state.shopingCart = payload
    })
    .addCase(actions.actionWishList, (state, { payload }) => {
      state.wishList = [...state.wishList, payload]
    })
    .addCase(actions.actionWishListLocalSor, (state, { payload }) => {
      state.wishList = payload
    })
    .addCase(actions.actionWishListRemove, (state, { payload }) => {
      const updateFavorites = state.wishList.filter(
        (item) => item.id !== payload.id,
      )
      state.wishList = [...updateFavorites]
    })
    .addCase(actions.actionShopingCartRemove, (state, { payload }) => {
      console.log(payload, 'payload')
      const updateShopingCart = state.shopingCart.filter(
        (item) => item.id !== payload.id,
      )
      console.log(updateShopingCart, 'updateShopingCart')
      state.shopingCart = updateShopingCart
    })
    .addCase(actions.addRedyToCard, (state, { payload }) => {
      state.addRedyToCard = [...state.addRedyToCard, payload]
    })
    .addCase(actions.addRemoveCard, (state, { payload }) => {
      state.addRemoveCard = payload
    })
    .addCase(actions.actionsObjectDelivery, (state, { payload }) => {
      state.objDelivery = payload
    })
})
