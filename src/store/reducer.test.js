import rootReducers, { initialState } from './reduce'
import * as actions from './actions'

describe('rootReducers', () => {
  test('should return the initial state', () => {
    expect(rootReducers(undefined, { type: undefined })).toEqual(initialState)
  })

  describe('reducer', () => {
    test('should return the initial state', () => {
      expect(rootReducers(undefined, { type: undefined })).toEqual(initialState)
    })

    test('should handle actionsProductData', () => {
      const payload = ['data1', 'data2']
      const newState = rootReducers(
        initialState,
        actions.actionsProductData(payload),
      )
      expect(newState.data).toEqual(payload)
    })

    test('should handle actionsOpenModal', () => {
      const payload = true
      const newState = rootReducers(
        initialState,
        actions.actionsOpenModal(payload),
      )
      expect(newState.modal).toBe(payload)
    })
    test('should handle actionsOpenModalForm', () => {
      const payload = true
      const newState = rootReducers(
        initialState,
        actions.actionsModalForm(payload),
      )
      expect(newState.modal2).toBe(payload)
    })
    test('should handle actionsWishList', () => {
      const payload = ['data1', 'data2']
      const newState = rootReducers(
        initialState,
        actions.actionWishList(payload),
      )
      expect(newState.wishList).toEqual([payload])
    })
    test('shold handel addRedyToCard', () => {
      const payload = ['data1', 'data2']
      const newState = rootReducers(
        initialState,
        actions.addRedyToCard(payload),
      )
      expect(newState.addRedyToCard).toEqual([payload])
    })
    test('shold handel addRemoveCard', () => {
      const payload = ['data1', 'data2']
      const newState = rootReducers(
        initialState,
        actions.addRemoveCard(payload),
      )
      expect(newState.addRemoveCard).toEqual(payload)
    })
    test('shold handel objDelivery', () => {
      const payload = {
        user: {
          name: 'dima',
          lastName: 'volkov',
          age: '21',
          adress: 'ssss',
          phone: '3333333',
        },
        product: ['data1', 'data2'],
      }
      const newState = rootReducers(
        initialState,
        actions.actionsObjectDelivery(payload),
      )
      expect(newState.objDelivery).toEqual(payload)
    })
  })
})
