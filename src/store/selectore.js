export const selectProductsData = (state) => state.data
export const selectShoppingCart = (state) => state.shopingCart
export const selectFavorite = (state) => state.wishList
export const selectLoader = (state) => state.modal
export const selectStateProducts = (state) => state
